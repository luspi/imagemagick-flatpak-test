#include <Magick++.h>
#include <MagickCore/MagickCore.h>

#include <list>
#include <iostream>

int main(int argc, char *argv[]) {

    const char* key1 = "MAGICK_CODER_MODULE_PATH=/app/lib/ImageMagick-7.1.1/modules-Q16HDRI/coders";
    const char* key2 = "MAGICK_FILTER_MODULE_PATH=/app/lib/ImageMagick-7.1.1/modules-Q16HDRI/filters";
    const char* key3 = "MAGICK_CONFIGURE_PATH=/app/lib/ImageMagick-7.1.1/config-Q16HDRI";

    putenv(const_cast<char*>(key1));
    putenv(const_cast<char*>(key2));
    putenv(const_cast<char*>(key3));

    Magick::InitializeMagick(nullptr);

    try {

        std::list<Magick::CoderInfo> coderList;
        Magick::coderInfoList(&coderList,           // Reference to output list
                              Magick::CoderInfo::TrueMatch, // Match readable formats
                              Magick::CoderInfo::AnyMatch,  // Don't care about writable formats
                              Magick::CoderInfo::AnyMatch); // Don't care about multi-frame support

        std::list<Magick::CoderInfo>::iterator entry = coderList.begin();
        while( entry != coderList.end() ) {

            std::cout << entry->name() << ": (" << entry->description() << ") : ";
            std::cout << "Readable = " << (entry->isReadable() ? "true" : "false") << ", ";
            std::cout << "Writable = " << (entry->isWritable() ? "true" : "false") << ", ";
            std::cout << "Multiframe = " << (entry->isMultiFrame() ? "true" : "false");
            std::cout << std::endl;
            entry ++;

        }

    } catch(Magick::Exception &e) {
        std::cout << "Magick failed: " << e.what() << std::endl;
        // do nothing here
    }

    return 0;
}
